Login Page -

Implemented validations are - 
User ID: required
Password: required, atleast 1 upper case, 1 lower case and 1 special character
Captcha: generate 5 digit random number locally.

Train booking home page-
From station: Autocomplete, required
to station: Autocomplete, required
Date: date picker

Train list Page - 
Table columns:
Train code, train name, arrival, departure, duration. - Implement sorting on all columns.
Option to book the seat

Add Passenger Page - 
Added passenger details.
Name, Age, gender, seat preference- Implemented validations and senior citizen logic

Payment Page - 
make payment via credit card
Implemented card detail validations, Captured otp

Success page with all details.