import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { City } from '../models/city.collection';
import { Product } from '../models/product.model';
import { TrainCollection } from '../models/train.collection';

// import { Product } from '../components/product/product.component';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  trainCollectionURL = "assets/dummy/train-collection.json";
  cityURL = "assets/dummy/cities.json";

  passengerInfo: BehaviorSubject<Object> = new BehaviorSubject<Object>({});


  constructor(private http: HttpClient) { }


  getTrainCollections(): Observable<TrainCollection[]> {
    return this.http.get<TrainCollection[]>(this.trainCollectionURL);
  }


  getCityCollections(): Observable<City[]> {
    return this.http.get<City[]>(this.cityURL);
  }

  setPassengerInfo(passengerInfo: Object) {
    this.passengerInfo.next(passengerInfo);
  }

  getPassengerInfo() {
    return this.passengerInfo.asObservable();
  }

  clearPassengerInfo() {
    return this.passengerInfo.next({});
  }


}


