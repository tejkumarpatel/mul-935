export interface Product {
    id: string;
    productImg: string;
    price: string;
    seller: Seller;
    rating: number;
    estimatedTime: string;
}

export interface Seller {
    name: string;
    shopName: string;
    Location: string;
    quantity: string;
    description: string;
}