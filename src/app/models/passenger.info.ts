export interface PassengerInfo {
    arrival: String,
    childrens: Children[],
    class: number,
    date: String,
    depature: String,
    destination: String,
    duration: String,
    km: number,
    passengers: Passenger[],
    source: String,
    tcode: String,
    tname: String,
    totalChildren: number,
    totalCost: number,
    totalPassenger: number,
    ticketCost:number
    serviceCost:number,
    discountCost:number
}

export interface Children {
    childrenName: String
    childrenAge: String
    childrenGender: String
}

export interface Passenger {
    passengerName: string;
    passengerAge: string;
    passengerGender: string;
    passengerSeat: string;
    passengerMeal: string;
    passengerSenior: string;
}