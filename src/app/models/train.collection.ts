export interface TrainCollection {
  id: number
  tcode: string
  tname: string
  fromPlace: string
  sourcePincode: string
  fromState: string
  depature: string
  toPlace: string
  toState: string
  destinationPincode: string
  arrival: string
  duration: string
  daily: boolean
  isSpecial: boolean
  days: Days
  quota: string[]
  seat: Seat
  distance: number
  date: string
  route: Array<any>
}

export interface Days {
  sunday: boolean
  monday: boolean
  tuesday: boolean
  wendesday: boolean
  thrusday: boolean
  friday: boolean
  saturday: boolean
}

export interface Seat {
  secondSeating: SecondSeating
  sleeperCoach: SleeperCoach
  thirdClass: ThirdClass
  secondClass: SecondClass
}

export interface SecondSeating {
  avail: Avail
  cost: number
  abbr: string
}

export interface Avail {
  GNWL: number
  RLWL: number
  AVL: number
}

export interface SleeperCoach {
  avail: Avail2
  cost: number
  abbr: string
}

export interface Avail2 {
  GNWL: number
  RLWL: number
  AVL: number
}

export interface ThirdClass {
  avail: Avail3
  cost: number
  abbr: string
}

export interface Avail3 {
  GNWL: number
  RLWL: number
  AVL: number
}

export interface SecondClass {
  avail: Avail4
  cost: number
  abbr: string
}

export interface Avail4 {
  GNWL: number
  RLWL: number
  AVL: number
}

export interface Route {
  Mahemdabad?: Mahemdabad
  Gothaj?: Gothaj
  Nadiad?: Nadiad
  BARODA?: Baroda
  Bharuch?: Bharuch
}

export interface Mahemdabad {
  fromTime: string
  toTime: string
}

export interface Gothaj {
  fromTime: string
  toTime: string
}

export interface Nadiad {
  fromTime: string
  toTime: string
}

export interface Baroda {
  fromTime: string
  toTime: string
}

export interface Bharuch {
  fromTime: string
  toTime: string
}
