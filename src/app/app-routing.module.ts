import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { HomeTrainBookComponent } from './components/home-train-book/home-train-book.component';
import { LoginComponent } from './components/login/login.component';
import { PassengerInfoComponent } from './components/passenger-info/passenger-info.component';
import { PaymentComponent } from './components/payment/payment.component';
import { SuccessBookingComponent } from './components/success-booking/success-booking.component';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';

import { TrainAvailableComponent } from './components/train-available/train-available.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'home', component: HomeTrainBookComponent
  },
  {
    path: 'train-list', component: TrainAvailableComponent
  },
  {
    path: 'passenger/:id/:cls', component: PassengerInfoComponent
  },
  {
    path: 'payment', component: PaymentComponent
  },
  {
    path: 'ticket-detail/:id/:cls', component: TicketDetailComponent
  },

  {
    path: 'success-booking', component: SuccessBookingComponent
  },
 
  {
    path: '**', component: PageNotFoundComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
