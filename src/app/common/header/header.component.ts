import { AfterContentInit, Component, DoCheck, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit,DoCheck,AfterContentInit {

  constructor(private router: Router) { }
  ngDoCheck(): void {
    this.checkToken();
  }

  ngOnInit(): void {
  }
  onNavigateToLogin() {
    this.router.navigate(['/login']);
    // console.log(sampleJson);
  }

  ngAfterContentInit(){
    this.checkToken();
  }

  token: boolean = false;
  checkToken() {
    if (localStorage.getItem("token") != null) {
      this.token = true;
    }
    else {
      this.token = false;
    }
  }

  onLogout(){
    console.log("Logout")
    localStorage.removeItem("token");
    this.router.navigate(['/login'])
  }
}
