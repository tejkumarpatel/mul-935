import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  valid: boolean = true;
  pInfoDate: string;
  pInfoAmount: String;
  constructor(private router: ActivatedRoute, private route: Router) { }
  creditCardForm: FormGroup;
  ngOnInit(): void {
    this.createCreditCardForm();
    this.router.params.subscribe((param: ParamMap) => {
      console.log(param['date']);
      console.log(param['totalCost']);
      this.pInfoAmount = param['totalCost'];
      this.pInfoDate = param['date'];
    })
  }
  createCreditCardForm() {
    this.creditCardForm = new FormGroup(
      {
        cardNumber: new FormControl(null, [Validators.required, Validators.pattern('[0-9]{12}')]),
        expiryDate: new FormControl(null, [Validators.required, Validators.pattern('[0-9]{1,2}/[0-9]{2}')]),
        cvvNumber: new FormControl(null, [Validators.required, Validators.pattern('[0-9]{3}')]),
        cardName: new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z ]+')]),
        otpNumber: new FormControl(null, [Validators.required, Validators.pattern('[0-9]{6}')]),
      }
    )
    // throw new Error('Method not implemented.');
  }
  onSubmitCardDetail(creditCardForm: FormGroup) {
    if (creditCardForm.valid) {
      this.valid = false;
      console.log(creditCardForm.value);
      creditCardForm.reset();
      this.route.navigate(['/success-booking']);
    }
  }

}
