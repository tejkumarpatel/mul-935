import { Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { City } from 'src/app/models/city.collection';
import { Children, Passenger, PassengerInfo } from 'src/app/models/passenger.info';
import { TrainCollection } from 'src/app/models/train.collection';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-passenger-info',
  templateUrl: './passenger-info.component.html',
  styleUrls: ['./passenger-info.component.css']
})
export class PassengerInfoComponent implements OnInit {

  constructor(private route: ActivatedRoute, private utilityService: UtilityService, private router: Router) { }
  trainId: number;
  trainClass: number;
  seniorMalePessengerCount = 0;
  seniorFeMalePessengerCount = 0;
  totalPassenger = 0;
  totalChildren = 0;
  totalCost = 0;
  ticketCost = 0;
  perCost = 0;
  serviceCost = 30;
  discountCost = 0;
  isProcessFurther: boolean = false;


  train: TrainCollection;
  passengerTicket: Array<Passenger> = [];

  childrenTicket: Array<Children> = [];

 
  passengerInfoForm: FormGroup;
  ngOnInit(): void {
    this.createPassengerInfoForm();
    this.getTrainId();
    this.getTrainFromCollection();
  }

  getTrainId() {
    this.route.params.subscribe((params: ParamMap) => {
      console.log(params['id']);
      this.trainId = params['id'];
      this.trainClass = params['cls'];
    });
  }

  getTrainFromCollection() {
    this.utilityService.getTrainCollections().subscribe((trains: TrainCollection[]) => {
      trains.map((tc: TrainCollection) => {
        if (tc.id == this.trainId) {
          this.train = tc;
          console.log(this.train);
        }
      })

    });
  }

  createPassengerInfoForm() {
    this.passengerInfoForm = new FormGroup({
      passengerArraay: new FormArray(
        [
          new FormGroup({
            passengerName: new FormControl(null, [Validators.required]),
            passengerAge: new FormControl(null, [Validators.required]),
            passengerGender: new FormControl('male', [Validators.required]),
            passengerSeat: new FormControl('No Preference', [Validators.required]),
            passengerMeal: new FormControl('veg', [Validators.required]),
            passengerSenior: new FormControl(null),
          }),

        ]
      ),
      childrenArray: new FormArray(
        [
          new FormGroup({
            childrenName: new FormControl(null),
            childrenAge: new FormControl(null),
            childrenGender: new FormControl('male'),
          }),
        ]
      )

    })
  }

  onAddPassenger() {
    (<FormArray>this.passengerInfoForm.get('passengerArraay')).push(new FormGroup({
      passengerName: new FormControl(null, [Validators.required]),
      passengerAge: new FormControl(null, [Validators.required]),
      passengerGender: new FormControl('male', [Validators.required]),
      passengerSeat: new FormControl('No Preference', [Validators.required]),
      passengerMeal: new FormControl('veg', [Validators.required]),
      passengerSenior: new FormControl(null),
    }));
  }
  onAddChildren() {
    (<FormArray>this.passengerInfoForm.get('childrenArray')).push(new FormGroup({
      childrenName: new FormControl('tej'),
      childrenAge: new FormControl(null),
      childrenGender: new FormControl(null),
    }));
  }

  proceed: boolean = false;
  onPassengerInfo(passengerInfoForm: FormGroup) {


    console.log(passengerInfoForm.valid);
    this.passengerTicket = [];
    this.isProcessFurther = false
    if (passengerInfoForm.valid) {
      this.proceed = true;
      console.log(passengerInfoForm.value);

      (<Array<{ childrenName: String, childrenAge: String, childrenGender: String }>>passengerInfoForm.value['childrenArray']).map(
        data => this.childrenTicket.push(data));
      (<Array<{
        passengerName: string,
        passengerAge: string
        passengerGender: string
        passengerSeat: string
        passengerMeal: string
        passengerSenior: string
      }>>passengerInfoForm.value['passengerArraay']).map(
        data => {
          if (data.passengerGender === "male" && parseInt(data.passengerAge) >= 60) {
            this.seniorMalePessengerCount = this.seniorMalePessengerCount + 1;
          }
          else if (data.passengerGender === "female" && parseInt(data.passengerAge) >= 58) {
            this.seniorFeMalePessengerCount = this.seniorFeMalePessengerCount + 1;
          }
          else {
            this.seniorMalePessengerCount = this.seniorMalePessengerCount;
            this.seniorFeMalePessengerCount = this.seniorFeMalePessengerCount;
          }

          this.passengerTicket.push(data);
        });

      this.totalPassenger = this.passengerTicket.length;
      this.totalChildren = this.childrenTicket.length;
      this.perCost = this.train.seat[this.trainClass].cost;
      this.ticketCost = this.perCost * this.passengerTicket.length;
      this.discountCost = this.perCost * 0.50 * this.seniorFeMalePessengerCount + this.perCost * 0.40 * this.seniorFeMalePessengerCount;
      this.totalCost = this.perCost * this.totalPassenger + this.serviceCost - this.discountCost;


      console.log("sm" + (this.perCost * 0.50 * this.seniorFeMalePessengerCount));
      console.log("sf" + (this.perCost * 0.40 * this.seniorFeMalePessengerCount));
      console.log("Cost" + this.train.seat[this.trainClass].cost);
      console.log("total Pas" + this.passengerTicket.length);



      let passengerInfo: PassengerInfo = {
        'tcode': this.train.tcode,
        'tname': this.train.tname,
        'totalPassenger': this.totalPassenger,
        'totalCost': this.totalCost,
        'passengers': this.passengerTicket,
        'childrens': this.childrenTicket,
        'totalChildren': this.totalChildren,
        'source': this.train.fromPlace,
        'destination': this.train.toPlace,
        'km': this.train.distance,
        'date': this.train.date,
        'duration': this.train.duration,
        'class': this.trainClass,
        'arrival': this.train.arrival,
        'depature': this.train.depature,
        'ticketCost': this.ticketCost,
        'serviceCost': this.serviceCost,
        'discountCost': this.discountCost
      }

      this.utilityService.setPassengerInfo(passengerInfo);
      this.router.navigate(['/ticket-detail', this.trainId, this.trainClass])
    }
    else {
      this.isProcessFurther = false;
    }
  }

}
