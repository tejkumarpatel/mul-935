import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { PassengerInfo } from 'src/app/models/passenger.info';
import { TrainCollection } from 'src/app/models/train.collection';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute, private utilityService: UtilityService, private router: Router) { }
  ticket: string;
  trainId: number;
  trainClass: number;
  train: TrainCollection;
  passengerInfo: PassengerInfo;
  ngOnInit(): void {
    // this.passengerInfo.passengers
    this.getTrainId();
    this.getTrainFromCollection();
    this.utilityService.getPassengerInfo().subscribe((data: PassengerInfo) => {
      console.log(data);
      this.passengerInfo = data;
    })
    console.log(this.ticket);
  }

  getTrainId() {
    this.route.params.subscribe((params: ParamMap) => {
      console.log(params['id']);
      this.trainId = params['id'];
      this.trainClass = params['cls'];
    });
  }

  getTrainFromCollection() {
    this.utilityService.getTrainCollections().subscribe((trains: TrainCollection[]) => {
      trains.map((tc: TrainCollection) => {
        if (tc.id == this.trainId) {
          this.train = tc;
          console.log(this.train);
        }
      })

    });
  }

  
  OnModify() {
  //  this._location.back();
    this.router.navigate(['/passenger',this.trainId,this.trainClass]);
  }
  OnPayment() {
    this.router.navigate(['/payment', { date: this.train.date, totalCost: this.passengerInfo.totalCost }]);
  }
}
