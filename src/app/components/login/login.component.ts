import { Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, DoCheck {

  leftNumber: number = Math.floor(Math.random() * 11);
  RightNumber: number = Math.floor(Math.random() * 11);
  totalNumber: number;
  constructor(private router: Router) { }
  userForm: FormGroup;
  ngOnInit(): void {
    this.generateCaptcha();
    this.createUserForm();
    this.totalNumber = this.leftNumber + this.RightNumber;
    // $(function () {
    //   $("#pid").datepicker();
    // });

  }
  ngDoCheck() {
    this.totalNumber = this.leftNumber + this.RightNumber;
  }

  generateCaptcha() {
    this.leftNumber = Math.floor(Math.random() * 110);
    this.RightNumber = Math.floor(Math.random() * 500)
  }
  onRefresh() {
    this.generateCaptcha();
  }
  onChangeCalled(e) {
    console.log("Change Called");
    console.log(e);
  }
  onClick(this) {
    console.log(this);
  }
  createUserForm() {
    this.userForm = new FormGroup({
      username: new FormControl(null, [Validators.required, Validators.pattern('[a-zA-Z0-9@#$]{3,}')]),
      userpassword: new FormControl(null, [Validators.required, Validators.pattern(/^(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#\$%\^&\*])(?=.{6,})/)
      ]),
      captchaNumber: new FormControl(null, Validators.required)
    })
    
  }


  onUserFormSubmit(userForm: FormGroup) {
    if (this.userForm.valid && this.userForm.controls['captchaNumber'].value == this.totalNumber.toString()) {
      console.log('Login');
      this.router.navigate(['/home']);
      localStorage.setItem("token", "token" + Math.random().toString());
    }

  }
}
