import { Component, OnInit } from '@angular/core';
import { PassengerInfo } from 'src/app/models/passenger.info';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-success-booking',
  templateUrl: './success-booking.component.html',
  styleUrls: ['./success-booking.component.css']
})
export class SuccessBookingComponent implements OnInit {

  constructor(private utilityService: UtilityService) { }

  passengerInfo: PassengerInfo;

  ngOnInit(): void {
    this.utilityService.getPassengerInfo().subscribe((data: PassengerInfo) => {
      console.log(data);
      this.passengerInfo = data;
    })
  }

}
