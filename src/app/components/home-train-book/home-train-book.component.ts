import { Component, DoCheck, OnInit } from '@angular/core';
import { City } from 'src/app/models/city.collection';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-home-train-book',
  templateUrl: './home-train-book.component.html',
  styleUrls: ['./home-train-book.component.css']
})
export class HomeTrainBookComponent implements OnInit, DoCheck {

  constructor(private utilityService: UtilityService) { }
  minDate: string;
  maxDate: string;
  keyword = 'name';
  cKeyword = 'class';
  sourceFocused: boolean = false;
  destinationFocused: boolean = false;
  classFocused: boolean = false;
  sourceItem: boolean = false;
  destinationItem: boolean = false;
  classItem: boolean = false;
  dateAvail: boolean = false;
  enableSearch: boolean = false;

  ngOnInit(): void {

    this.setMinDate();
    this.utilityService.getCityCollections().subscribe((cities: City[]) => {
      cities.map(city => this.junctions.push(city));
    })
    console.log(this.junctions);
  }


  ngDoCheck() {

  }



  setMinDate() {
    let today = new Date();
    let tmonth = today.getMonth();
    let tdate = today.getDate();
    let month = tmonth <= 10 ? "0" + (tmonth + 1).toString() : tmonth;
    let day = tdate <= 10 ? ("0" + tdate.toString()) : tdate;
    let tyear = today.getFullYear();
    let mindate = today.getFullYear() + '-' + (month) + '-' + day;
    let maxdate = (tyear + 1) + '-' + month + '-' + day;
    this.minDate = mindate;
    this.maxDate = maxdate;
    document.getElementById("myDate1").setAttribute("min", mindate);
    document.getElementById("myDate1").setAttribute("max", maxdate);
    // document.getElementById("myDate").min="2020-08-21";

  }


  public classTypes = [
    { name: "AC1", id: 1 },
    { name: "AC2", id: 2 },
    { name: "SL", id: 3 },
    { name: "2S", id: 4 },
  ]



  public junctions = [
  ];

  onDateInput() {
    this.dateAvail = true;
  }
  searchResult(){

    if (this.sourceItem && this.destinationItem && this.classItem) {
      this.enableSearch = true;
      console.log("enabled");
    }
    else {
      this.enableSearch = false;
    }
  }

  sourceselectEvent(item: boolean) {
    console.log("Select");
    if (item) {
      this.sourceItem = true
    }
    else {
      this.sourceItem = false
    }
  }
  destinationselectEvent(item: boolean) {
    console.log("Select");
    if (item) {
      this.destinationItem = true
    }
    else {
      this.destinationItem = false
    }
  } 
  classselectEvent(item: boolean) {
    console.log("Select");
    if (item) {
      this.classItem = true
    }
    else {
      this.classItem = false
    }
  }

  onSourceChangeSearch(search: string) {

    this.sourceItem = true;
    console.log("Seach change");
  }
  onDestinationChangeSearch(search: string) {

    this.destinationItem = true;
  }
  onClassChangeSearch(search: string) {

    this.classItem = true;
  }

  onSourceFocused(e) {

    this.sourceFocused = true;
  }

  onDestinationFocused(e) {

    this.destinationFocused = true;
  }

  onClassFocused(e) {

    this.classFocused = true;
  }


}
