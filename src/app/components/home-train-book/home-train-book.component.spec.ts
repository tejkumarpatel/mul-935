import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTrainBookComponent } from './home-train-book.component';

describe('HomeTrainBookComponent', () => {
  let component: HomeTrainBookComponent;
  let fixture: ComponentFixture<HomeTrainBookComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeTrainBookComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTrainBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
