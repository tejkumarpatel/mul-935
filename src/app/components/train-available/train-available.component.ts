import { KeyValue } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TrainCollection } from 'src/app/models/train.collection';
import { UtilityService } from 'src/app/services/utility.service';

@Component({
  selector: 'app-train-available',
  templateUrl: './train-available.component.html',
  styleUrls: ['./train-available.component.css']
})
export class TrainAvailableComponent implements OnInit {
  tcodeasec: boolean = true;
  depasec: boolean = true;
  arrasec: boolean = true;
  durasec: boolean = true;

  orderType: string = "asec";
  orderBasedType: string = "tcode";
  ontcodeAscClick(value: string) {
    this.orderBasedType = value
    this.tcodeasec = !this.tcodeasec;
    this.orderType = this.tcodeasec ? "asec" : "desc";

  }

  ondepAscClick(value: string) {
    this.orderBasedType = value;
    this.depasec = !this.depasec;
    this.orderType = this.depasec ? "asec" : "desc";
  }
  onarrAscClick(value: string) {
    this.orderBasedType = value;
    this.arrasec = !this.arrasec;
    this.orderType = this.arrasec ? "asec" : "desc";
  }
  ondurAscClick(value: string) {
    this.orderBasedType = value;
    this.durasec = !this.durasec;
    this.orderType = this.durasec ? "asec" : "desc";
  }


  constructor(private utilityService: UtilityService, private router: Router) { }
  p: number = 1;

  orderOriginal = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0
  }
  orderbyValueAsc = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return a.value > b.value ? -1 : (a.value > b.value) ? 0 : 1
  }

  orderbyValueDsc = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return a.value > b.value ? 1 : (a.value > b.value) ? 0 : -1
  }

  searchText: string;

  trains: TrainCollection[] = [];
  ngOnInit(): void {

    this.utilityService.getTrainCollections().subscribe(
      (collections: TrainCollection[]) => {
        console.log(collections);
        collections.map((train: TrainCollection) => {
          this.trains.push(train);
        });
      }
    );
  }
  onNavigateeToPassengerInfo(train: TrainCollection, cls: string) {

    this.router.navigate(['/passenger', train.id, cls]);
  }
}
