import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common/header/header.component';
import { LoginComponent } from './components/login/login.component';
import { HomeTrainBookComponent } from './components/home-train-book/home-train-book.component';
import { TrainAvailableComponent } from './components/train-available/train-available.component';
import { PassengerInfoComponent } from './components/passenger-info/passenger-info.component';
import { PaymentComponent } from './components/payment/payment.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NgxPaginationModule } from 'ngx-pagination';
import { SuccessBookingComponent } from './components/success-booking/success-booking.component';
import { TicketDetailComponent } from './components/ticket-detail/ticket-detail.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { OrderByPipe } from './pipes/order-by.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeTrainBookComponent,
    TrainAvailableComponent,
    PassengerInfoComponent,
    PaymentComponent,
    PageNotFoundComponent,
    SuccessBookingComponent,
    TicketDetailComponent,
    OrderByPipe,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    AutocompleteLibModule,
    NgxPaginationModule,
    Ng2SearchPipeModule
  ],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
