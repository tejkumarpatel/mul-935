import { Pipe, PipeTransform } from '@angular/core';
import { TrainCollection } from '../models/train.collection';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {

  transform(value: TrainCollection[], btype: string, type: string): any {
    // console.log(value);

    console.log(btype);
    console.log(type);
    switch (btype) {
      case "tcode":
        switch (type) {
          case "asec":
            console.log('hi');
            return value.sort((a, b) => a.tcode > b.tcode ? 1 : -1);
          case "desc":
            console.log('hello');
            return value.sort((a, b) => a.tcode > b.tcode ? -1 : 1);
        }
      case "dep":
        switch (type) {
          case "asec":
            return value.sort((a, b) => a.depature > b.depature ? 1 : -1);
          case "desc":
            return value.sort((a, b) => a.depature > b.depature ? -1 : 1);
        }
      case "arr":
        switch (type) {
          case "asec":
            return value.sort((a, b) => a.arrival > b.arrival ? 1 : -1);
          case "desc":
            return value.sort((a, b) => a.arrival > b.arrival ? -1 : 1);
        }
      case "dur":
        switch (type) {
          case "asec":
            return value.sort((a, b) => a.duration > b.duration ? 1 : -1);
          case "desc":
            return value.sort((a, b) => a.duration > b.duration ? -1 : 1);
        }
      default:
        return value;
    }

    // let r = [
    //   { id: 123233434, tcode: "01242" },
    //   { id: 123233435, tcode: "01322" },
    // ].sort((a, b) => (a['id'] > b['id'] ? 1 : 0));
    // console.log(r);
    return value;
  }

}
